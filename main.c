/*	Segment A  - PD3	Active Low
	Segment B  - PD0	Active Low
	Segment C  - PB6	Active Low
	Segment D  - PB7	Active Low
	Segment E  - PD5	Active Low
	Segment F  - PD4	Active Low
	Segment G  - PD1	Active Low
	DP         - PD6	Active Low
	A1         - PD7	Active Low
	A2         - PC5	Active Low
	*
	* Rundenzähler
*/

#ifndef F_CPU
#define F_CPU 1000000UL // interner RC-Oszillator mit 1 Mhz
#endif
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/power.h>

#define SEG0_ON		PORTC &= ~(1<<PC5); PORTD |= (1<<PD7)
#define	SEG1_ON		PORTC |= (1<<PC5); PORTD &= ~(1<<PD7)
#define SEG_OFF		PORTC |= (1<<PC5); PORTD |= (1<<PD7)

const uint8_t seg7_PB[10] = {0b00111111, 0b10111111, 0b01111111, 0b00111111, 0b10111111, 0b00111111, 0b00111111, 0b10111111, 0b00111111, 0b00111111};  //array für die anoden der 7segmentanzeigen
const uint8_t seg7_PD[10] = {0b11000110, 0b11111110, 0b11010100, 0b11110100, 0b11101100, 0b11100101, 0b11000101, 0b11110110, 0b11000100, 0b11100100};

volatile uint8_t seg[2] = {0, 0}; //in seg stehen was links und rechts angezeigt werden soll
volatile uint8_t counterVal = 0;
volatile uint32_t timer = 0;
volatile uint8_t wakeup = 0;   //wakeup = 1 ^= gerade aufgewacht -> nicht hochzählen

void setSeg7(uint8_t val, uint8_t dp) {
	if(dp) PORTD &= 0b10111111;
	else PORTD |= 0b01000000;
	
	if(val > 9) {
		//alle Segmente aus
		PORTB |= 0b11000000;
		PORTD |= 0b00111011;
		return;
	}
	
	PORTB = (PORTB | 0b11000000) & seg7_PB[val];
	PORTD = (PORTD | 0b00111011) & seg7_PD[val];
}

int main(void) {
	SEG_OFF;

	//Ausgänge für Anzeigen
	DDRB = (1<<PB6) | (1<<PB7);
	DDRC = (1<<PC5);
	DDRD = (1<<PD0) | (1<<PD1) | (1<<PD3) | (1<<PD4) | (1<<PD5) | (1<<PD6) | (1<<PD7);

	//Pullup für Taster an PD2
	PORTD |= (1<<PD2);

	TCCR0B |= (1<<CS00); //Timer 0, kein Prescaler
	TIMSK0 |= (1<<TOIE0); //Timer/Counter0 Overflow Interrupt Enable

	clock_prescale_set(clock_div_1);
	sei();

	while(1) {
		seg[0] = counterVal%10;
		seg[1] = (counterVal/10)%10;
		if(timer > 14000) {            // Timer für Sleep
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			EIMSK |= (1<<INT0);			//INT0 enable
			TCCR0B &= ~(1<<CS00);		//Timer0 disable
			SEG_OFF;
			wakeup = 1;				
			sleep_mode();
			EIMSK &= ~(1<<INT0);		//INT0 disable
			TCCR0B |= (1<<CS00);		//Timer 0 enable
			timer = 0;
		}
	}
	return 1;
}

ISR(TIMER0_OVF_vect) {
	static uint8_t activeSeg = 0;
	static uint16_t buttonPressed = 0;
	uint8_t dp = 0;
	SEG_OFF;
	if(activeSeg == 0) {
		if(counterVal >= 100) dp = 1;
		setSeg7(seg[0], dp);
		SEG0_ON;
	}
	else if(activeSeg == 1) {
		if(counterVal >= 200) dp = 1;
		if(seg[1] == 0) setSeg7(10, dp);
		else setSeg7(seg[1], dp);
		SEG1_ON;
	}
	activeSeg++;
	if(activeSeg > (timer/312)%10) activeSeg = 0;
	//kein kommentaru
	//featureA!
	//if(timer < 40000) activeSeg &= 0b00000111;      // Überlauf nach 7, für Dimmung der Anzeige
	//else activeSeg &= 0b01111111;
	//master

	if(!(PIND & (1<<PD2))) {   //button ist gedrückt
		if(buttonPressed < 32000) buttonPressed++;   // kleiner "zeit" dann zurücksetzen
		else counterVal = 0;  // counter wird gelöscht nach der "zeit"
		//timer = 0;
	}
	else if(buttonPressed != 0) {   //Button nicht gedrückt
		if(buttonPressed > 400 && buttonPressed < 32000 && wakeup == 0) {
			counterVal++;   // knopf kurz gedrückt gewesen,
			timer = 0;
		}
		wakeup = 0;          // nach aufwachen und knopf loslassen wird zähler null gesetzt und hochzählen bei aktivem chip möglich
		buttonPressed = 0;
	}

	timer++;
}

ISR(INT0_vect) {
	
}
